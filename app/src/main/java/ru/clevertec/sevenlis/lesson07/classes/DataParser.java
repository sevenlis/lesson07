package ru.clevertec.sevenlis.lesson07.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import ru.clevertec.sevenlis.lesson07.meta.FieldsItem;
import ru.clevertec.sevenlis.lesson07.meta.MetaResponse;
import ru.clevertec.sevenlis.lesson07.meta.Value;

public class DataParser {

    public JSONObject parseJsonObject(MetaResponse metaResponse) throws JSONException {

        JSONObject formObj = new JSONObject();
        List<FieldsItem> fieldsItems = metaResponse.getFields();
        for (FieldsItem fieldsItem : fieldsItems) {
            Value selectedValue = fieldsItem.getSelectedValue();
            formObj.putOpt(selectedValue.getKey(), selectedValue.getValue());
        }

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("form", formObj);

        return jsonObj;
    }

    public String parseResult(String jsonResult) {
        String result;
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            result = jsonObject.getString("result");
        } catch (JSONException e) {
            e.printStackTrace();
            result = jsonResult + "\n" + e.getMessage();
        }
        return result;
    }
}
