package ru.clevertec.sevenlis.lesson07.meta;

import java.io.Serializable;
import java.util.List;

public class MetaResponse implements Serializable {
	private final String title;
	private final String image;
	private final List<FieldsItem> fields;

	public MetaResponse(String title, String image, List<FieldsItem> fields) {
		this.title = title;
		this.image = image;
		this.fields = fields;
	}

	public String getTitle() {
		return title;
	}

	public String getImage() {
		return image;
	}

	public List<FieldsItem> getFields() {
		return fields;
	}
}