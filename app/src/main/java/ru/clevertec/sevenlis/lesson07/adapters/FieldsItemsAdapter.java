package ru.clevertec.sevenlis.lesson07.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.clevertec.sevenlis.lesson07.R;
import ru.clevertec.sevenlis.lesson07.meta.FieldType;
import ru.clevertec.sevenlis.lesson07.meta.FieldsItem;
import ru.clevertec.sevenlis.lesson07.meta.Value;
import ru.clevertec.sevenlis.lesson07.ui.EditValueDialog;

public class FieldsItemsAdapter extends RecyclerView.Adapter<FieldsItemsAdapter.ViewHolder> implements EditValueDialog.Listener {
    private final Context mContext;
    private final List<FieldsItem> fieldsItems;

    public FieldsItemsAdapter(Context mContext, List<FieldsItem> fieldsItems) {
        this.mContext = mContext;
        this.fieldsItems = fieldsItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fields_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final FieldsItem fieldsItem = fieldsItems.get(position);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editFieldValue(holder, fieldsItem);
            }
        };

        holder.textViewName.setText(fieldsItem.getTitle());
        holder.textViewValue.setText(fieldsItem.getSelectedValue().getName());
        holder.textViewValue.setOnClickListener(onClickListener);
        holder.imageButtonEdit.setOnClickListener(onClickListener);
    }

    private void editFieldValue(ViewHolder holder, FieldsItem fieldsItem) {
        if (fieldsItem.getType() == FieldType.LIST) {
            final List<Value> values = fieldsItem.getValues();
            DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int position) {
                    if (position != AdapterView.INVALID_POSITION) {
                        Value value = values.get(position);
                        fieldsItem.setSelectedValue(new Value(fieldsItem.getName(), value.getKey(), value.getName()));
                        holder.textViewValue.setText(fieldsItem.getSelectedValue().getName());
                        dialogInterface.dismiss();
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(fieldsItem.getTitle());
            ArrayAdapter<Value> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.select_dialog_singlechoice, values);
            builder.setSingleChoiceItems(arrayAdapter, AdapterView.INVALID_POSITION, onClickListener).create().show();
        } else {
            EditValueDialog editValueDialog = new EditValueDialog(mContext, fieldsItem, this);
            editValueDialog.create();
            editValueDialog.show();
        }
    }

    @Override
    public int getItemCount() {
        return fieldsItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onPositiveClick(FieldsItem fieldsItem) {
        notifyItemChanged(fieldsItems.indexOf(fieldsItem));
    }

    @Override
    public void onNegativeClick() {

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewValue;
        ImageButton imageButtonEdit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.name);
            textViewValue = itemView.findViewById(R.id.value);
            imageButtonEdit = itemView.findViewById(R.id.button_edit);
        }
    }
}
