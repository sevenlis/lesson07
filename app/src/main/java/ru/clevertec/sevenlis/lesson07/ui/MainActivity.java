package ru.clevertec.sevenlis.lesson07.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.clevertec.sevenlis.lesson07.R;
import ru.clevertec.sevenlis.lesson07.adapters.FieldsItemsAdapter;
import ru.clevertec.sevenlis.lesson07.classes.MetaParser;
import ru.clevertec.sevenlis.lesson07.classes.Progress;
import ru.clevertec.sevenlis.lesson07.components.DaggerApiServiceComponent;
import ru.clevertec.sevenlis.lesson07.classes.DataParser;
import ru.clevertec.sevenlis.lesson07.meta.FieldsItem;
import ru.clevertec.sevenlis.lesson07.meta.MetaResponse;
import ru.clevertec.sevenlis.lesson07.services.ApiService;

public class MainActivity extends AppCompatActivity {
    private MetaResponse metaResponse;
    private Progress progress;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiService = DaggerApiServiceComponent.builder().build().getApiService();

        ProgressBar progressBar = findViewById(R.id.progress_bar);
        progress = new Progress(progressBar);

        if (savedInstanceState == null) {
            getMeta();
        } else {
            metaResponse = (MetaResponse) savedInstanceState.getSerializable("metaResponse");
            setViews(metaResponse);
            progress.stop();
        }

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("metaResponse", metaResponse);
    }

    private void setViews(MetaResponse metaResponse) {
        Button buttonSend = findViewById(R.id.button_submit);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (metaResponse != null) {
                    sendData();
                }
            }
        });
        buttonSend.setVisibility(View.GONE);

        ImageView imageView = findViewById(R.id.image_view);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        if (metaResponse != null) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(metaResponse.getTitle());
            }

            RequestOptions requestOptions = new RequestOptions().placeholder(R.mipmap.ic_launcher_round).fitCenter().error(R.mipmap.ic_launcher_round);
            Glide.with(this).load(metaResponse.getImage()).apply(requestOptions).into(imageView);

            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

            List<FieldsItem> fieldsItems = metaResponse.getFields();
            FieldsItemsAdapter fieldsItemsAdapter = new FieldsItemsAdapter(this, fieldsItems);
            recyclerView.setAdapter(fieldsItemsAdapter);

            buttonSend.setVisibility(View.VISIBLE);
        }
    }

    private void getMeta() {
        progress.start();

        apiService.loadMeta().enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                try {
                    metaResponse = new MetaParser().parse(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                    onFailure(call, e);
                }
                setViews(metaResponse);
                progress.stop();
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                showResult("Ошибка:", t.getMessage());
                setViews(metaResponse);
                progress.stop();
            }
        });
    }

    private void sendData() {
        progress.start();

        JSONObject jsonObject;
        try {
            jsonObject = new DataParser().parseJsonObject(metaResponse);
        } catch (JSONException e) {
            e.printStackTrace();
            showResult("Ошибка:", e.getMessage());
            progress.stop();
            return;
        }

        apiService.sendData(jsonObject.toString()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                String result = new DataParser().parseResult(response.body());
                showResult("Результат:", result);
                progress.stop();
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                showResult("Ошибка:", t.getMessage());
                progress.stop();
            }
        });
    }

    private void showResult(String title, String result) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(result)
                .setPositiveButton(android.R.string.yes, null)
                .create()
                .show();
    }
}