package ru.clevertec.sevenlis.lesson07.components;

import dagger.Component;
import ru.clevertec.sevenlis.lesson07.annotations.ApiServiceScope;
import ru.clevertec.sevenlis.lesson07.modules.ApiServiceModule;
import ru.clevertec.sevenlis.lesson07.services.ApiService;

@ApiServiceScope
@Component(modules = {ApiServiceModule.class})
public interface ApiServiceComponent {
    ApiService getApiService();
}
