package ru.clevertec.sevenlis.lesson07.meta;

import java.io.Serializable;

public enum FieldType implements Serializable {
    TEXT,
    NUMERIC,
    LIST
}
