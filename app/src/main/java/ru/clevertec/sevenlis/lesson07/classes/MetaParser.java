package ru.clevertec.sevenlis.lesson07.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.clevertec.sevenlis.lesson07.meta.FieldType;
import ru.clevertec.sevenlis.lesson07.meta.FieldsItem;
import ru.clevertec.sevenlis.lesson07.meta.MetaResponse;
import ru.clevertec.sevenlis.lesson07.meta.Value;

public class MetaParser {

    public MetaResponse parse(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        return new MetaResponse(
                jsonObject.getString("title"),
                jsonObject.getString("image"),
                getFields(jsonObject)
        );
    }

    private List<FieldsItem> getFields(JSONObject jsonObject) {
        List<FieldsItem> fieldsItems = new ArrayList<>();
        JSONArray jsonArray = jsonObject.optJSONArray("fields");
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject arrayItem = jsonArray.optJSONObject(i);
                if (arrayItem != null) {
                    fieldsItems.add(getFieldsItem(arrayItem));
                }
            }
        }
        return fieldsItems;
    }

    private FieldsItem getFieldsItem(JSONObject arrayItem) {
        String title = arrayItem.optString("title");
        String name = arrayItem.optString("name");
        FieldType type = getFieldType(arrayItem.optString("type"));
        List<Value> values = null;
        if (type == FieldType.LIST) {
            JSONObject valuesObject = arrayItem.optJSONObject("values");
            if (valuesObject != null) {
                values = getValues(valuesObject);
            }
        }
        return new FieldsItem(title, name, type, values);
    }

    private List<Value> getValues(JSONObject valuesObject) {
        ArrayList<Value> values = new ArrayList<>();
        JSONArray names = valuesObject.names();
        if (names != null) {
            for (int i = 0; i < names.length(); i++) {
                String key = names.optString(i);
                String value = valuesObject.optString(key);
                values.add(new Value(key, value, value));
            }
        }
        return values;
    }

    private FieldType getFieldType(String type) {
        switch (type) {
            case "TEXT":
                return FieldType.TEXT;
            case "NUMERIC":
                return FieldType.NUMERIC;
            case "LIST":
                return FieldType.LIST;
            default:
                return null;
        }
    }
}
