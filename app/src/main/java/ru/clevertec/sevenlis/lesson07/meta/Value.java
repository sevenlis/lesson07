package ru.clevertec.sevenlis.lesson07.meta;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Value implements Serializable {
    private final String key;
    private final String value;
    private final String name;

    public Value(String key, String value, String name) {
        this.key = key;
        this.value = value;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return getName();
    }
}
