package ru.clevertec.sevenlis.lesson07.meta;

import java.io.Serializable;
import java.util.List;

public class FieldsItem implements Serializable {
	private final String title;
	private final String name;
	private final FieldType type;
	private final List<Value> values;

	private Value selectedValue;

	public FieldsItem(String title, String name, FieldType type, List<Value> values) {
		this.title = title;
		this.name = name;
		this.type = type;
		this.values = values;
		this.selectedValue = new Value(name, "", "");
	}

	public String getTitle() {
		return title;
	}

	public String getName() {
		return name;
	}

	public FieldType getType() {
		return type;
	}

	public List<Value> getValues() {
		return values;
	}

	public void setSelectedValue(Value value) {
		this.selectedValue = value;
	}

	public Value getSelectedValue() {
		return selectedValue;
	}
}