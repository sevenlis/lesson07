package ru.clevertec.sevenlis.lesson07.services;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {
    @GET("meta/")
    Call<String> loadMeta();

    @POST("data/")
    @Headers({"Content-Type: application/json; charset=utf-8;"})
    Call<String> sendData(@Body String json);
}
