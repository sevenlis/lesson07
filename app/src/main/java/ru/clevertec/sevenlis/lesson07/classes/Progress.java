package ru.clevertec.sevenlis.lesson07.classes;

import android.view.View;
import android.widget.ProgressBar;

public class Progress {
    private final ProgressBar progressBar;

    public Progress(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public void start() {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void stop() {
        progressBar.setVisibility(View.GONE);
    }
}
