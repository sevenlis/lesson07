package ru.clevertec.sevenlis.lesson07.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.clevertec.sevenlis.lesson07.R;
import ru.clevertec.sevenlis.lesson07.meta.FieldType;
import ru.clevertec.sevenlis.lesson07.meta.FieldsItem;
import ru.clevertec.sevenlis.lesson07.meta.Value;

public class EditValueDialog extends Dialog {

    public interface Listener {
        void onPositiveClick(FieldsItem fieldsItem);
        void onNegativeClick();
    }

    private FieldsItem fieldsItem;
    private Listener mListener;
    private Context mContext;
    private EditText editTextValue;

    public EditValueDialog(@NonNull Context context) {
        super(context);
    }

    public EditValueDialog(@NonNull Context context, FieldsItem fieldsItem, Listener mListener) {
        this(context);
        this.fieldsItem = fieldsItem;
        this.mListener = mListener;
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.edit_value_dialog);

        TextView textViewTitle = findViewById(R.id.title);
        textViewTitle.setText(fieldsItem.getTitle());

        editTextValue = findViewById(R.id.value);
        if (fieldsItem.getType() == FieldType.NUMERIC) {
            editTextValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        } else {
            editTextValue.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        if (fieldsItem.getSelectedValue() != null) {
            editTextValue.setText(fieldsItem.getSelectedValue().getValue());
        }
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        editTextValue.requestFocus();

        Button buttonSave = findViewById(R.id.button_yes);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Value value = getValue(editTextValue.getText().toString());
                if (value != null) {
                    fieldsItem.setSelectedValue(value);
                    mListener.onPositiveClick(fieldsItem);
                    hideSoftInputKeyboard(editTextValue);
                    dismiss();
                }
            }
        });

        Button buttonCancel = findViewById(R.id.button_no);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onNegativeClick();
                hideSoftInputKeyboard(editTextValue);
                dismiss();
            }
        });
    }

    private Value getValue(String s) {
        if (fieldsItem.getType() == FieldType.NUMERIC) {
            s = s.replace(",", ".").replace(" ", "");

            String regex = "(-?\\d+(\\.\\d+)?)";
            if (!s.matches(regex)) {
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(s);
                if (matcher.find()) {
                    s = s.substring(matcher.start(), matcher.end());
                }
            }

            try {
                Double.parseDouble(s);
            } catch (NumberFormatException e) {
                Toast.makeText(mContext, "Введенное значение не может быть преобразовано в число!", Toast.LENGTH_SHORT).show();
                return null;
            }
        }
        return new Value(fieldsItem.getName(), s, s);
    }

    private void hideSoftInputKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
